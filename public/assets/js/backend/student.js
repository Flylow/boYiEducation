define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'student/index' + location.search,
                    add_url: 'student/add',
                    // edit_url: 'student/edit',
                    // del_url: 'student/del',
                    multi_url: 'student/multi',
                    table: 'student',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false},
                        {field: 'user_name', title: __('User_name')},
                        {field: 'nick_name', title: __('Nick_name'), operate: false},
                        {field: 'phone', title: __('Phone')},
                        {field: 'grade', title: __('Grade')},
                        {field: 'school', title: __('School'), operate: false},
                        {field: 'age', title: __('Age'), operate: false},
                        {
                            field: 'sex',
                            title: __('Sex'),
                            searchList: {"man": __('Sex man'), "woman": __('Sex woman')},
                            formatter: Table.api.formatter.normal,
                            operate: false
                        },
                        // {field: 'password', title: __('Password')},
                        {
                            field: 'header_image',
                            title: __('Header_image'),
                            events: Table.api.events.image,
                            formatter: Table.api.formatter.image,
                            operate: false
                        },
                        {field: 'course_name', title: __('Course_name'), operate: false},
                        {field: 'plan', title: __('Plan'), operate: false},
                        {field: 'point', title: __('Point'), operate: false},
                        {field: 'invite_code', title: __('Invite_code'), operate: false},
                        {
                            field: 'register_time',
                            title: __('Register_time'),
                            operate: false,
                            addclass: 'datetimerange',
                        },
                        {
                            field: 'operate', title: __('Operate'), table: table,
                            buttons:[
                                {
                                    name: "查看详情",
                                    text: "查看详情",
                                    icon: '',
                                    classname: 'btn btn-xs btn-primary btn-dialog diseare',
                                    url: function (row, column) {
                                        return "student/edit/ids/" + row.id;
                                    },
                                    extend: 'data-area=\'["90%", "90%"]\'',
                                    visible: function (row) {
                                            return true;
                                    }
                                },
                                {
                                    name: "查看注册积分记录",
                                    text: "查看注册积分记录",
                                    icon: '',
                                    classname: 'btn btn-xs btn-primary btn-dialog diseare',
                                    url: function (row, column) {
                                        return "Investearnings/indexs/ids/" + row.id;
                                    },
                                    extend: 'data-area=\'["90%", "90%"]\'',
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return group !== 2;
                                    }
                                },
                            ],
                            events: Table.api.events.operate, formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});