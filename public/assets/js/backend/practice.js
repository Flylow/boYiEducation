define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'practice/index' + location.search,
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: 'practice/multi',
                    table: 'practice',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',title: __('Id'),visible:false,operate: false},
                        {field: 'type_status', title: __('Type_status'), searchList: {"1":__('Type_status 1'),"2":__('Type_status 2'),"3":__('Type_status 3')}, formatter: Table.api.formatter.status},
                        {field: 'nick_name', title: __('Nick_name'),operate: false},
                        {field: 'subject_name', title: __('Subject_name')},
                        {field: 'difficulty', title: __('Difficulty'),searchList:{'1' : __('Difficulty 1'), '2' : __('Difficulty 2'), '3' : __('Difficulty 3'), '4' : __('Difficulty 4'), '5' : __('Difficulty 5')}, formatter: Table.api.formatter.normal},
                        {field: 'grade_name', title: __('Grade')},
                        {field: 'create_time', title: __('Create_time'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table,
                            buttons:[
                                {
                                    name: "查看详情",
                                    text: "查看详情",
                                    classname: 'btn btn-info btn-xs btn-primary btn-dialog',
                                    url: function (row, column) {
                                        return "practice/edit/ids/" + row.id;
                                    },
                                    extend: 'data-area=\'["95%", "95%"]\'',
                                    callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        // console.log(data);//控制输出回调数据
                                    },
                                    visible: function (row) {
                                        // var group = $("#group").data('id');
                                        // return row.is_check === '1' && group === 1;
                                        return true;
                                    }
                                },
                            ],
                            events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});