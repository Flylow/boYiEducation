define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            var courseID = $("#courseID").data('id');

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'question_bank/index/ids/' + courseID  + location.search,
                    add_url: 'question_bank/add/ids/' + courseID,
                    edit_url: 'question_bank/edit',
                    del_url: 'question_bank/del',
                    multi_url: 'question_bank/multi',
                    table: 'question_bank',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'course.title', title: __('Course.title')},
                        {field: 'courselesson.title', title: __('Courselesson.title')},
                        {field: 'grade.grade_name', title: __('Grade.grade_name')},
                        {field: 'subject.subject_name', title: __('Subject.subject_name')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'bank_tags', title: __('Bank_tags')},
                        {field: 'bank_status', title: __('BankStatus'), searchList: {"0":__('BankStatus 0'),"1":__('BankStatus 1')}, formatter: Table.api.formatter.normal},
                        {field: 'operate', title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate,
                            buttons:[{
                                name: 'addtabs',
                                text: __('添加题目'),
                                classname: 'btn btn-xs btn-warning btn-addtabs',
                                icon: 'fa fa-folder-o',
                                url: 'question/index'
                            }],

                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});