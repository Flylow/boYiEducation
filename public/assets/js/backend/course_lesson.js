define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            var courseID = $("#courseID").data('id');

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'course_lesson/index/ids/' + courseID + location.search,
                    add_url: 'course_lesson/add/ids/' + courseID,
                    edit_url: '',
                    del_url: 'course_lesson/del',
                    multi_url: 'course_lesson/multi',
                    table: 'course_lesson',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'lesson_id',
                sortOrder: 'ASC',
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'id', title: __('Id'),},
                        {field: 'course.title', title: __('Course.title')},
                        {field: 'lesson_id', title: __('Lesson_id')},
                        {field: 'title', title: __('Title')},
                        {
                            field: 'lesson_type',
                            title: __('Lesson_type'),
                            searchList: {"document": __('Lesson_type document'), "video": __('Lesson_type video')},
                            formatter: Table.api.formatter.normal
                        },
                        {
                            field: 'lesson_status',
                            title: __('Lesson_status'),
                            searchList: {
                                "0": __('Lesson_status 0'),
                                "1": __('Lesson_status 1'),
                                "2": __('Lesson_status 2')
                            },
                            formatter: Table.api.formatter.status
                        },
                        {
                            field: 'start_time',
                            title: __('Start_time'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            visible: false
                        },
                        {
                            field: 'end_time',
                            title: __('End_time'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            visible: false
                        },
                        {
                            field: 'operate', title: __('Operate'), table: table,
                            buttons: [
                                {
                                    name: "发布课时",
                                    text: "发布课时",
                                    icon: '',
                                    classname: 'btn btn-info btn-xs btn-primary btn-ajax',
                                    url: function (row, column) {
                                        return "course_lesson/issue/type/1/ids/" + row.id;
                                    },
                                    confirm: '是否确定发布该课时',
                                    success: function () {
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                    },
                                    error: function () {
                                        // return false;
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return (group === 2 && (row.lesson_status !== '1'));
                                    }
                                },
                                {
                                    name: "取消发布",
                                    text: "取消发布",
                                    icon: '',
                                    classname: 'btn btn-warning btn-xs btn-primary btn-ajax',
                                    url: function (row, column) {
                                        return "course_lesson/issue/type/2/ids/" + row.id;
                                    },
                                    confirm: '是否确定取消发布该课时',
                                    success: function () {
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                    },
                                    error: function () {
                                        // return false;
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return (group === 2 && (row.lesson_status === '1'));
                                    }
                                },
                                {
                                    name: 'addtabs',
                                    text: __('资源管理'),
                                    classname: 'btn btn-xs btn-warning btn-addtabs',
                                    icon: 'fa fa-book ',
                                    url: 'Course_lesson_resource/index'
                                },
                                {
                                    name: "编辑",
                                    text: "编辑",
                                    icon: 'edit',
                                    classname: 'btn btn-info btn-xs btn-primary btn-dialog',
                                    url: function (row, column) {
                                        return "course_lesson/edit/ids/" + row.id;
                                    },
                                    extend: 'data-area=\'["95%", "95%"]\'',
                                    callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        // console.log(data);//控制输出回调数据
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        var is_check = $("#is_check").data('id');
                                        return group === 2 && is_check !== 2;
                                    }
                                },
                                {
                                    name: "查看",
                                    text: "查看",
                                    classname: 'btn btn-info btn-xs btn-primary btn-dialog',
                                    url: function (row, column) {
                                        return "course_lesson/show/ids/" + row.id;
                                    },
                                    extend: 'data-area=\'["95%", "95%"]\'',
                                    callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        // console.log(data);//控制输出回调数据
                                    }
                                },
                                {
                                    name: "去直播",
                                    text: "去直播",
                                    classname: 'btn btn-danger btn-xs btn-primary btn-ajax',
                                    url: function (row, column) {
                                        return "course_lesson/beginLive/ids/" + row.id;
                                    },
                                    confirm: '是否确定开始直播',
                                    success: function (data, ret) {
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        window.open(ret.url);
                                    },
                                    error: function (data, ret) {
                                        console.log(ret);
                                        // return false;
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return group === 2 && row.live === 1;
                                    }
                                }
                            ],
                            events: Table.api.events.operate, formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            $(document).on("change", '.lesson_type', function () {
                var val = $('input:radio[name="row[lesson_type]"]:checked').val();
                if (val === 'document') {
                    $(".title,.contents,.minlength").show("slow");
                    $(".mediauri,.video_length,.starttime,.endtime").hide("slow");
                } else if (val === 'video') {
                    $(".contents,.starttime,.endtime").hide("slow");
                    $(".mediauri,.minlength").show("slow");
                } else if (val === 'live') {
                    $(".starttime,.endtime,.title,.contents").show("slow");
                    $(".mediauri,.minlength").hide("slow");
                }
            });

            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        show: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});