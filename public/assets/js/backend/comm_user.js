define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'comm_user/index' + location.search,
                    add_url: 'comm_user/add',
                    edit_url: '',
                    del_url: '',
                    multi_url: 'comm_user/multi',
                    table: 'comm_user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('id'),operate: false},
                        {field: 'uname', title: __('Uname')},
                        {field: 'course.title', title: __('Course.title')},
                        {field: 'content', title: __('Content'),operate: false},
                        {field: 'time', title: __('Time'), operate:'RANGE', addclass:'datetimerange',},
                        {field: 'status', title: __('Status'), searchList: {"on":__('Status on'),"off":__('Status off')}, formatter: Table.api.formatter.status},
                        // {field: 'applied', title: __('Applied'), searchList: {"0":__('Applied 0'),"1":__('Applied 1')}, formatter: Table.api.formatter.status},
                        // {field: 'type', title: __('Type'), searchList: {"course":__('Type course'),"chapter":__('Type chapter')}, formatter: Table.api.formatter.normal},
                        {field: 'level', title: __('Level'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table,
                                buttons:[
                                    {
                                        name: "审核",
                                        text: "审核",
                                        icon: 'edit',
                                        classname: 'btn btn-danger btn-xs btn-primary btn-dialog',
                                        url: function (row, column) {
                                            return "comm_user/edit/ids/" + row.id;
                                        },
                                        extend: 'data-area=\'["95%", "95%"]\'',
                                        callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                            $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        }
                                    },
                                    {
                                        name: "回复",
                                        text: "回复",
                                        icon: 'add',
                                        classname: 'btn btn-info btn-xs btn-primary btn-dialog',
                                        url: function (row, column) {
                                            return "comm_user/add/ids/" + row.id;
                                        },
                                        extend: 'data-area=\'["80%", "80%"]\'',
                                        callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                            $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        },
                                        visible: function (row) {
                                            return  true;
                                            // return row.applied === '0';
                                        }
                                    },
                                    {
                                        name: "查看",
                                        text: "查看",
                                        classname: 'btn btn-info btn-xs btn-primary btn-dialog',
                                        url: function (row, column) {
                                            return "comm_user/show/ids/" + row.id;
                                        },
                                        extend: 'data-area=\'["50%", "60%"]\'',
                                        callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                            $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        },
                                        visible: function (row) {
                                            return false;
                                            // return row.applied === '1';
                                        }
                                    },
                                ],
                            events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});