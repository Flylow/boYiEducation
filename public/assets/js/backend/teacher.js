define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'teacher/index' + location.search,
                    add_url: 'teacher/add',
                    edit_url: '',
                    del_url: '',
                    multi_url: 'teacher/multi',
                    table: 'teacher',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate: false},
                        // {field: 'name', title: __('Name')},
                        {field: 'phone', title: __('Phone')},
                        // {field: 'password', title: __('Password')},
                        {field: 'nick_name', title: __('Nick_name')},
                        {field: 'header', title: __('Header'),operate: false, formatter: Table.api.formatter.image},
                        // {field: 'point', title: __('Point')},
                        {field: 'sex', title: __('Sex'), searchList: {"man":__('Sex man'),"woman":__('Sex woman')}, formatter: Table.api.formatter.normal},
                        {field: 'age', title: __('Age'),operate: false},
                        // {field: 'last_login_time', title: __('Last_login_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'new_message_num',operate: false, title: __('New_message_num')},
                        {field: 'new_notification_num',operate: false, title: __('New_notification_num')},
                        {field: 'register_time', title: __('Register_time'), operate:false, addclass:'datetimerange'},
                        {field: 'invite_code', title: __('Invite_code'),operate: false},
                        // {field: 'english_name', title: __('English_name')},
                        {field: 'birthday', title: __('Birthday'), operate:false, addclass:'datetimerange'},
                        // {field: 'address', title: __('Address')},
                        {field: 'grade', title: __('Grade'),operate: false},
                        {field: 'school', title: __('School'),operate: false},
                        // {field: 'admin_id', title: __('Admin_id')},
                        {field: 'auth_status', title: __('Auth_status'), searchList: {"0":__('Auth_status 0'),"1":__('Auth_status 1'),"2":__('Auth_status 2'),"3":__('Auth_status 3'),"4":__('Auth_status 4')}, formatter: Table.api.formatter.status},
                        {field: 'create_time', title: __('Create_time'), operate:false, addclass:'datetimerange'},
                        {field: 'auth_time', title: __('Auth_time'), operate:false, addclass:'datetimerange'},
                        {field: 'operate', title: __('Operate'),
                            buttons:[
                                {
                                    name: "审核",
                                    text: "审核",
                                    icon: 'edit',
                                    classname: 'btn btn-danger btn-xs btn-primary btn-dialog',
                                    url: function (row, column) {
                                        return "teacher/edit/ids/" + row.id;
                                    },
                                    extend: 'data-area=\'["95%", "95%"]\'',
                                    callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        // console.log(data);//控制输出回调数据
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return row.auth_status !== '1' && group === 1;
                                    }
                                },
                            ],
                            table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            $(document).on("change",'input:radio[name="row[auth_status]"]',function (){
                var val=$('input:radio[name="row[auth_status]"]:checked').val();
                if(val !== '1'){
                    $(".auth-reason").show("slow");
                }else{
                    $(".auth-reason").hide("slow");
                    $("#c-auth-reason").val('');
                }
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});