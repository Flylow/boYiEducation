define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'investearnings/index' + location.search,
                    add_url: 'investearnings/add',
                    edit_url: 'investearnings/edit',
                    del_url: 'investearnings/del',
                    multi_url: 'investearnings/multi',
                    table: 'investearnings',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'invest_id', title: __('Invest_id')},
                        {field: 'earnings', title: __('Earnings'), operate:'BETWEEN'},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'order_id', title: __('Order_id')},
                        {field: 'type', title: __('Type'), searchList: {"1":__('Type 1'),"2":__('Type 2')}, formatter: Table.api.formatter.normal},
                        {field: 'invest.id', title: __('Invest.id')},
                        {field: 'invest.uid', title: __('Invest.uid')},
                        {field: 'invest.role', title: __('Invest.role')},
                        {field: 'invest.to_uid', title: __('Invest.to_uid')},
                        {field: 'invest.to_role', title: __('Invest.to_role')},
                        {field: 'invest.to_nick_name', title: __('Invest.to_nick_name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});