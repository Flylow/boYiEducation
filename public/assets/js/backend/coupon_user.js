define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'coupon_user/index' + location.search,
                    add_url: 'coupon_user/add',
                    edit_url: 'coupon_user/edit',
                    del_url: 'coupon_user/del',
                    multi_url: 'coupon_user/multi',
                    table: 'coupon_user',
                }
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'coupon_id',
                sortName: 'coupon_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'coupon_id', title: __('Coupon_id')},
                        {field: 'student.nick_name', title: __('Student.nick_name')},
                        {field: 'name', title: __('Name')},
                        {field: 'sn', title: __('Sn')},
                        {field: 'status', title: __('Status'), searchList: {"1":__('Status 1'),"0":__('Status 0'),"2":__('Status 2'),"3":__('Status 3')}, formatter: Table.api.formatter.status},
                        {field: 'money', title: __('Money'), operate:'BETWEEN'},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});