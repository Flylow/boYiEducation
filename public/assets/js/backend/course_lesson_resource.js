define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            var lessonID = $("#lessonID").data('id');
            Table.api.init({
                extend: {
                    index_url: 'course_lesson_resource/index/ids/' + lessonID + location.search,
                    add_url: 'course_lesson_resource/add/ids/' + lessonID,
                    edit_url: 'course_lesson_resource/edit',
                    del_url: 'course_lesson_resource/del',
                    multi_url: 'course_lesson_resource/multi',
                    table: 'course_lesson_resource',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        // {field: 'lesson_id', title: __('Lesson_id')},
                        {field: 'type', title: __('Type')},
                        {field: 'location', title: __('Location')},
                        {field: 'size', title: __('Size')},
                        {field: 'remark', title: __('Remark')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'courselesson.title', title: __('Courselesson.title')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});