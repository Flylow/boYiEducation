define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {

        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'helper/index' + location.search,
                    add_url: 'helper/add',
                    edit_url: '',
                    del_url: '',
                    multi_url: 'helper/multi',
                    table: 'helper',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false},
                        // {field: 'name', title: __('Name'),operate: false},
                        {field: 'teacher_id', title: __('Teacher_Id'), operate: false},
                        {field: 'phone', title: __('Phone')},
                        {field: 'nick_name', title: __('Nick_name')},
                        {field: 'header', title: __('Header'), formatter: Table.api.formatter.image, operate: false},
                        {field: 'money', title: __('Money'), operate: false},
                        {field: 'age', title: __('Age'), operate: false},
                        {
                            field: 'last_login_time',
                            title: __('Last_login_time'),
                            operate: false,
                            addclass: 'datetimerange'
                        },
                        // {field: 'new_message_num', title: __('New_message_num'), operate: false},
                        // {field: 'new_notification_num', title: __('New_notification_num'), operate: false},
                        {field: 'create_time', title: __('Create_time'), operate: false, addclass: 'datetimerange'},
                        {field: 'invite_code', title: __('Invite_code')},
                        {field: 'english_name', title: __('English_name'), operate: false},
                        {
                            field: 'account_status', title: __('Account_Status'),
                            searchList: {"0": __('Account_Status 0'), "1": __('Account_Status 1')},
                            formatter: Table.api.formatter.status,
                            custom: {frozen: 'gray'}
                        },
                        {
                            field: 'operate', title: __('Operate'), table: table,
                            buttons: [
                                {
                                    name: "审核",
                                    text: "审核",
                                    icon: 'edit',
                                    classname: 'btn btn-danger btn-xs btn-primary btn-dialog',
                                    url: function (row, column) {
                                        return "helper/edit/ids/" + row.id;
                                    },
                                    extend: 'data-area=\'["95%", "95%"]\'',
                                    callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        // console.log(data);//控制输出回调数据
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return row.account_status === '0' && group === 1;
                                    }
                                },
                                {
                                    name: "冻结",
                                    text: "冻结",
                                    icon: '',
                                    classname: 'btn btn-danger btn-xs btn-primary btn-ajax',
                                    url: function (row, column) {
                                        return "helper/edit/account_status/0/ids/" + row.id;
                                    },
                                    confirm:'是否确定冻结该助教',
                                    success:function(){
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                    },
                                    error:function () {
                                        // return false;
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return (group === 2 && row.account_status === '1');
                                    }
                                },
                                {
                                    name: "解冻",
                                    text: "解冻",
                                    icon: '',
                                    classname: 'btn btn-info btn-xs btn-primary btn-ajax',
                                    url: function (row, column) {
                                        return "helper/edit/account_status/1/ids/" + row.id;
                                    },
                                    confirm:'是否确定解冻该助教',
                                    success:function(){
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                    },
                                    error:function () {
                                        // return false;
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return (group === 2 && row.account_status === '0');
                                    }
                                }
                            ],
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            $(document).on("click", "#pl-code", function () {
                var phone = $("#c-phone").val();
                sendCode(phone);
            });

            function sendCode(phone) {
                if (phone === '') {
                    Toastr.error('手机号码不可为空');
                    return;
                }
                if ((!(/^1[3456789]\d{9}$/.test(phone)))) {
                    Toastr.error('手机号码格式错误');
                    return;
                }
                Fast.api.ajax({
                    url: '/api/Sms/send',
                    data: {mobile: phone, event: 'register'},
                    method: 'POST',
                    loading: false
                }, function (data, dev) {
                    if (dev.code === 1) {
                        Toastr.success(dev.msg);
                        settime();
                    }
                    return false;
                }, function (data, dev) {
                    Toastr.error(dev.msg);
                    return false;
                });
            }

            var countdown = 60;
            function settime() {
                if (countdown == 0) {
                    $("#pl-code").attr("disabled", false);
                    $("#pl-code").text("获取验证码");
                    countdown = 60;
                } else {
                    $("#pl-code").attr("disabled", true);
                    $("#pl-code").text("重新发送(" + countdown + ")");
                    countdown--;
                    setTimeout(settime, 1000);
                }
            }

            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});