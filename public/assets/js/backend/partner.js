define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'partner/index' + location.search,
                    add_url: 'partner/add',
                    edit_url: 'partner/edit',
                    del_url: 'partner/del',
                    multi_url: 'partner/multi',
                    table: 'partner',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate: false},
                        // {field: 'type', title: __('Type'), searchList: {"province":__('Type province'),"city":__('Type city'),"county":__('Type county')}, formatter: Table.api.formatter.normal},
                        {field: 'nick_name', title: __('Nick_name')},
                        {field: 'phone', title: __('Phone')},
                        {field: 'account_status', title: __('Account_Status'),
                            searchList: {"0":__('Account_Status 0'),"1":__('Account_Status 1')},
                            formatter: Table.api.formatter.status,
                            custom: {frozen:'gray'}
                        },
                        {field: 'header', title: __('Header'),operate: false,formatter: Table.api.formatter.image},
                        {field: 'money', title: __('Money'),operate: false},
                        {field: 'sex', title: __('Sex'), searchList: {"man":__('Sex man'),"woman":__('Sex woman')}, formatter: Table.api.formatter.normal},
                        {field: 'age', title: __('Age'),operate: false},
                        // {field: 'last_login_time', title: __('Last_login_time'), operate:false, addclass:'datetimerange'},
                        // {field: 'new_message_num', title: __('New_message_num'),operate: false},
                        // {field: 'new_notification_num', title: __('New_notification_num'),operate: false},
                        {field: 'register_time', title: __('Register_time'), operate:false, addclass:'datetimerange'},
                        {field: 'invite_code', title: __('Invite_code'),operate: false},
                        {field: 'remark', title: __('Remark'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});