define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $(document).on("click",".nav-title li",function (){
                $(this).addClass("active");
                $(this).siblings('li').removeClass('active');
                let className = $(this).data('id');
                $(".presentation").hide();
                $("." + className).show();
            });
            $(document).on("click",".nav-sales li",function (){
                $(this).addClass("active");
                $(this).siblings('li').removeClass('active');
                let className = $(this).data('id');
                $(".sale").hide();
                $("." + className).show();
            });
            Controller.api.bindevent();

        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});