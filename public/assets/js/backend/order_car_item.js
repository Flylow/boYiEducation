define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'order_car_item/index' + location.search,
                    add_url: 'order_car_item/add',
                    edit_url: 'order_car_item/edit',
                    del_url: 'order_car_item/del',
                    multi_url: 'order_car_item/multi',
                    table: 'order_car_item',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'student_id', title: __('Student_id')},
                        {field: 'course_id', title: __('Course_id')},
                        {field: 'course_name', title: __('Course_name')},
                        {field: 'remark', title: __('Remark')},
                        {field: 'course.title', title: __('Course.title')},
                        {field: 'course.subtitle', title: __('Course.subtitle')},
                        {field: 'course.cover_image', title: __('Course.cover_image'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        // {field: 'course.content', title: __('Course.content')},
                        // {field: 'course.tache', title: __('Course.tache')},
                        // {field: 'course.duration', title: __('Course.duration')},
                        // {field: 'course.teacher_manifesto', title: __('Course.teacher_manifesto')},
                        {field: 'course.teacher_id', title: __('Course.teacher_id')},
                        // {field: 'course.help_id', title: __('Course.help_id')},
                        // {field: 'course.grade_id', title: __('Course.grade_id')},
                        // {field: 'course.weigh', title: __('Course.weigh')},
                        // {field: 'course.difficulty', title: __('Course.difficulty')},
                        // {field: 'course.course_num', title: __('Course.course_num')},
                        // {field: 'course.section_num', title: __('Course.section_num')},
                        // {field: 'course.student_num', title: __('Course.student_num')},
                        // {field: 'course.comment_num', title: __('Course.comment_num')},
                        // {field: 'course.hit_num', title: __('Course.hit_num')},
                        // {field: 'course.type_status', title: __('Course.type_status'), formatter: Table.api.formatter.status},
                        {field: 'course.income', title: __('Course.income'), operate:'BETWEEN'},
                        {field: 'course.tag_ids', title: __('Course.tag_ids')},
                        {field: 'course.subject_id', title: __('Course.subject_id')},
                        {field: 'course.classify_type', title: __('Course.classify_type')},
                        {field: 'course.is_free', title: __('Course.is_free')},
                        {field: 'course.price', title: __('Course.price'), operate:'BETWEEN'},
                        {field: 'course.is_promotion', title: __('Course.is_promotion')},
                        {field: 'course.is_issue', title: __('Course.is_issue')},
                        {field: 'course.is_check', title: __('Course.is_check')},
                        {field: 'course.check_remark', title: __('Course.check_remark')},
                        {field: 'course.issue_time', title: __('Course.issue_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'course.promotion_price', title: __('Course.promotion_price'), operate:'BETWEEN'},
                        {field: 'course.is_apply', title: __('Course.is_apply')},
                        {field: 'course.apply_start_time', title: __('Course.apply_start_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'course.apply_end_time', title: __('Course.apply_end_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'course.expiry_start_time', title: __('Course.expiry_start_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'course.expiry_end_time', title: __('Course.expiry_end_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'course.create_time', title: __('Course.create_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'course.update_time', title: __('Course.update_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'student.phone', title: __('Student.phone')},
                        {field: 'student.nick_name', title: __('Student.nick_name')},
                        {field: 'student.sex', title: __('Student.sex')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});