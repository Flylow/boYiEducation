define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'ad/index' + location.search,
                    add_url: 'ad/add',
                    edit_url: 'ad/edit',
                    del_url: '',
                    multi_url: 'ad/multi',
                    table: 'ad',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'title', title: __('Course.title')},
                        {field: 'pic_url', title: __('Pic_url'), formatter: Table.api.formatter.image,operate: false},
                        {field: 'ad_comment', title: __('Ad_comment'),operate: false},
                        {field: 'create_time', title: __('Create_time'), operate:false, addclass:'datetimerange'},
                        // {field: 'create_user', title: __('Create_user'),operate: false},
                        {field: 'status', title: __('Status'), searchList: {"off":__('Status off'),"on":__('Status on'),"delete":__('Status delete')}, formatter: Table.api.formatter.status},
                        {field: 'location', title: __('Location'), searchList: {"top":__('Location top'),"bottom":__('Location bottom'),"left":__('Location left'),"right":__('Location right')}, formatter: Table.api.formatter.normal},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});