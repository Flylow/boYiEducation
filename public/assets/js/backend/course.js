define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'course/index' + location.search,
                    add_url: 'course/add',
                    edit_url: '',
                    del_url: 'course/del',
                    multi_url: 'course/multi',
                    table: 'course',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate: false},
                        {field: 'title', title: __('Title'),operate: false},
                        {field: 'course_num', title: __('Course_num'),operate: false},
                        {field: 'section_num', title: __('Section_num'),operate: false},
                        {field: 'difficulty', title: __('Difficulty'),searchList:{'1' : __('Difficulty 1'), '2' : __('Difficulty 2'), '3' : __('Difficulty 3'), '4' : __('Difficulty 4'), '5' : __('Difficulty 5')}, formatter: Table.api.formatter.normal},
                        {field: 'type_status', title: __('Type_status'), searchList: {"1":__('Type_status 1'),"2":__('Type_status 2'),"3":__('Type_status 3')}, formatter: Table.api.formatter.status},
                        {field: 'price', title: __('Price'), operate:false},
                        {field: 'is_issue', title: __('Is_issue'), searchList: {"0":__('Is_issue 0'),"1":__('Is_issue 1')}, formatter: Table.api.formatter.normal},
                        {field: 'is_check', title: __('Is_check'), searchList: {"0":__('Is_check 0'),"1":__('Is_check 1'),"2":__('Is_check 2'),"3":__('Is_check 3')}, formatter: Table.api.formatter.normal},
                        {field: 'issue_time', title: __('Issue_time'), operate:false, addclass:'datetimerange'},
                        {field: 'teacher.name', title: __('Teacher.name'),operate: false},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange',operate: false},
                        {field: 'helper.nick_name', title: __('Helper.name'),operate: false},
                        {field: 'classify.name', title: __('Classify.name'),operate: false},
                        {field: 'tag.tag_name', title: __('Tag.tag_name'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table,
                            buttons:[
                                {
                                    name: "编辑",
                                    text: "编辑",
                                    icon: 'edit',
                                    classname: 'btn btn-info btn-xs btn-primary btn-dialog',
                                    url: function (row, column) {
                                        return "course/edit/ids/" + row.id;
                                    },
                                    extend: 'data-area=\'["95%", "95%"]\'',
                                    callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        // console.log(data);//控制输出回调数据
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return  (row.is_check ==='0' || row.is_check ==='3') && group === 2  ;
                                    }
                                },
                                {
                                    name: "提交审核",
                                    text: "提交审核",
                                    icon: '',
                                    classname: 'btn btn-info btn-xs btn-primary btn-ajax',
                                    url: function (row, column) {
                                        return "course/putCheck/ids/" + row.id;
                                    },
                                    confirm:'是否确定提交审核',
                                    success:function(){
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                    },
                                    error:function () {
                                        // return false;
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return  (row.is_check ==='0' || row.is_check ==='3') && group === 2 ;
                                    }
                                },
                                {
                                    name: "审核",
                                    text: "审核",
                                    icon: 'edit',
                                    classname: 'btn btn-danger btn-xs btn-primary btn-dialog',
                                    url: function (row, column) {
                                        return "course/check/ids/" + row.id;
                                    },
                                    extend: 'data-area=\'["95%", "95%"]\'',
                                    callback: function (data) {            //回调方法，用来响应 Fast.api.close()方法 **注意不能有success 是btn-ajax的回调，btn-dialog 用的callback回调，两者不能同存！！！！
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                        // console.log(data);//控制输出回调数据
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return row.is_check === '1' && group === 1;
                                    }
                                },
                                {
                                    name: "发布课时",
                                    text: "发布课时",
                                    icon: '',
                                    classname: 'btn btn-info btn-xs btn-primary btn-ajax',
                                    url: function (row, column) {
                                        return "course/teacherIssue/is_issue/1/ids/" + row.id;
                                    },
                                    confirm:'是否确定发布该课时',
                                    success:function(){
                                        $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                    },
                                    error:function () {
                                        // return false;
                                    },
                                    visible: function (row) {
                                        var group = $("#group").data('id');
                                        return (group === 2 && (row.is_issue !== '1' && row.is_check === '2'));
                                    }
                                },
                                // {
                                //     name: "取消发布",
                                //     text: "取消发布",
                                //     icon: '',
                                //     classname: 'btn btn-warning btn-xs btn-primary btn-ajax',
                                //     url: function (row, column) {
                                //         return "course_lesson/issue/type/2/ids/" + row.id;
                                //     },
                                //     confirm:'是否确定取消发布该课时',
                                //     success:function(){
                                //         $(".btn-refresh").trigger("click");//刷新当前页面的数据
                                //     },
                                //     error:function () {
                                //         // return false;
                                //     },
                                //     visible: function (row) {
                                //         var group = $("#group").data('id');
                                //         return (group === 2 && (row.is_issue === '1' && row.is_check === '2'));
                                //     }
                                // },
                                {
                                    name: 'addtabs',
                                    text: __('课时管理'),
                                    classname: 'btn btn-xs btn-warning btn-addtabs',
                                    icon: 'fa fa-book ',
                                    url: 'Course_lesson/index',
                                    // visible: function (row) {
                                    //     var group = $("#group").data('id');
                                    //     return group === 2;
                                    // }
                                },
                                {
                                    name: 'addtabs',
                                    text: __('题库管理'),
                                    classname: 'btn btn-xs btn-warning btn-addtabs',
                                    icon: 'fa fa-book ',
                                    url: 'question_bank/index',
                                }
                            ],
                            events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        check:function (){
            $(function (){
                $('input').attr("readonly","readonly");
                $('#c-check_remark').attr("readonly",false);
            });
            $(document).on("change",".is_check",function (){
                var val=$('input:radio[name="row[is_check]"]:checked').val();
                if(val === '3'){
                    $(".check_remark").show("slow");
                    //不通过
                }else{
                    $(".check_remark").hide("slow");
                    $('#c-check_remark').val('');
                    //通过
                }
            });

            Controller.api.bindevent();
        },
        add: function () {
            $(document).on('change', '#c-is_free', function () {
                var value = $(this).val();
                if(value === 0){
                    $(".price").toggle('slow');
                    $(".promotion").toggle('slow');
                }else{
                    $(".price").toggle('slow').val(0);
                    $(".promotion").toggle('slow');
                }
            });
            $(document).on("change","#c-is_promotion",function (){
                var value = $(this).val();
                if(value === 0){
                    $(".promotion_price").toggle('slow');
                }else{
                    $(".promotion_price").toggle('slow').val(0);
                }
            });
            $(document).on("change",".is_trail",function (){
                var val=$('input:radio[name="row[is_trail]"]:checked').val();
                if(val === '1'){
                    $(".mediauri").show("slow");
                    //不通过
                }else{
                    //通过
                    $(".mediauri").hide("slow");
                    $("#c-trial_video").val('');
                }
            });
            $(document).on("change","#c-is_discount",function (){
                var val = $(this).val();
                if(val === '1'){
                    $(".discount").show("slow");
                }else{
                    $(".discount").hide("slow");
                    $("#c-discount").val('');
                }
            });
            $(document).on("change",".type_status",function (){
                var val=$('input:radio[name="row[type_status]"]:checked').val();
                if(val === '2' || val === '3'){
                    $(".help_id,.course_num").hide("slow");
                }else{
                    $(".help_id,.course_num").show("slow");
                }
            });
            Controller.api.bindevent();
        },
        edit: function () {
            $(document).on('change', '#c-is_free', function () {
                var value = $(this).val();
                if(value === 0){
                    $(".price").toggle('slow');
                    $(".promotion").toggle('slow');
                }else{
                    $(".price").toggle('slow').val(0);
                    $(".promotion").toggle('slow');
                }
            });
            $(document).on("change","#c-is_promotion",function (){
                var value = $(this).val();
                if(value === 0){
                    $(".promotion_price").toggle('slow');
                }else{
                    $(".promotion_price").toggle('slow').val(0);
                }
            });
            $(document).on("change",".is_trail",function (){
                var val=$('input:radio[name="row[is_trail]"]:checked').val();
                if(val === '1'){
                    $(".mediauri").show("slow");
                    //不通过
                }else{
                    //通过
                    $(".mediauri").hide("slow");
                    $("#c-trial_video").val('');
                }
            });
            $(document).on("change","#c-is_discount",function (){
                var val = $(this).val();
                if(val === '1'){
                    $(".discount").show("slow");
                }else{
                    $(".discount").hide("slow");
                    $("#c-discount").val('');
                }
            });
            $(document).on("change",".type_status",function (){
                var val=$('input:radio[name="row[type_status]"]:checked').val();
                if(val === '2' || val === '3'){
                    $(".help_id,.course_num").hide("slow");
                }else{
                    $(".help_id,.course_num").show("slow");
                }
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});