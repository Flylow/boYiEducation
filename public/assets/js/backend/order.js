define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'order/index' + location.search,
                    // add_url: 'order/add',
                    // edit_url: 'order/edit',
                    // del_url: 'order/del',
                    multi_url: 'order/multi',
                    table: 'order',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate:false},
                        {field: 'batch_id', title: __('Batch_id')},
                        {field: 'student.phone', title: __('Phone')},
                        {field: 'student.nick_name', title: __('Student.nick_name')},
                        {field: 'status', title: __('Status'), searchList: {"created":__('Status created'),"paid":__('Status paid'),"refunding":__('Status refunding'),"refunded":__('Status refunded'),"cancelled":__('Status cancelled'),"lock":__('Status lock')}, formatter: Table.api.formatter.status},
                        {field: 'title', title: __('Title'), operate:false},
                        {field: 'amount', title: __('Amount'), operate:false},
                        {field: 'coupon', title: __('Coupon'), operate:false},
                        {field: 'payment', title: __('Payment'), searchList: {"alipay":__('Payment alipay'),"wechat":__('Payment wechat'),"none":__('Payment none')}, formatter: Table.api.formatter.normal},
                        {field: 'price_type', title: __('Price_type'), searchList: {"RMB":__('Price_type rmb'),"Coin":__('Price_type coin')}, formatter: Table.api.formatter.normal, operate:false},
                        {field: 'paid_time', title: __('Paid_time'), operate:false, addclass:'datetimerange'},
                        {field: 'cash_sn', title: __('Cash_sn')},
                        {field: 'note', title: __('Note'), operate:false},
                        {field: 'refund_end_time', title: __('Refund_end_time'), operate:false, addclass:'datetimerange'},
                        {field: 'create_time', title: __('Create_time'), operate:false, addclass:'datetimerange'},
                        {field: 'cashflow.amount', title: __('Cashflow.amount'), operate:false},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});