define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            var ids = $("#ids").data('id');
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'question/index/ids/'+ ids + location.search,
                    add_url: 'question/add/ids/'+ids,
                    edit_url: 'question/edit',
                    del_url: 'question/del',
                    multi_url: 'question/multi',
                    table: 'question',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'type', title: __('Type'), searchList: {"just":__('Type just'),"choose":__('Type choose'),"mchoose":__('Type mchoose')}, formatter: Table.api.formatter.normal},
                        {field: 'q_info', title: __('Info')},
                        {field: 'difficulty', title: __('Difficulty'), searchList: {"hard":__('Difficulty hard'),"mid":__('Difficulty mid'),"easy":__('Difficulty easy')}, formatter: Table.api.formatter.normal},
                        {field: 'score', title: __('Score')},
                        {field: 'answer', title: __('Answer')},
                        {field: 'questionbank.name', title: __('Questionbank.name')},
                        {field: 'q_status', title: __('QStatus'), searchList: {"0":__('QStatus 0'),"1":__('QStatus 1')}, formatter: Table.api.formatter.normal},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});