<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 1对1学习预约
 *
 * @icon fa fa-circle-o
 */
class Onebyone extends Backend
{
    
    /**
     * Onebyone模型对象
     * @var \app\admin\model\Onebyone
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Onebyone;

    }
}
