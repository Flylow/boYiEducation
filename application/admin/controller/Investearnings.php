<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 分销收益记录
 *
 * @icon fa fa-circle-o
 */
class Investearnings extends Backend
{
    
    /**
     * InvestEarnings模型对象
     * @var \app\admin\model\InvestEarnings
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\InvestEarnings;
        $this->view->assign("typeList", $this->model->getTypeList());
    }

    public function indexs($ids = "")
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->with(['invest'])
//                    ->where($where1)
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with(['invest'])
//                    ->where($where1)
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $row) {
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch('index');
    }
}
