<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 购物车课程
 *
 * @icon fa fa-circle-o
 */
class OrderCarItem extends Backend
{
    
    /**
     * OrderCarItem模型对象
     * @var \app\admin\model\OrderCarItem
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\OrderCarItem;

    }
    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['course','student'])
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['course','student'])
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                $row->visible(['id','student_id','course_id','course_name','remark']);
                $row->visible(['course']);
				$row->getRelation('course')->visible(['title','subtitle','cover_image','content','tache','duration','teacher_manifesto','teacher_id','help_id','grade_id','weigh','difficulty','course_num','section_num','student_num','comment_num','hit_num','type_status','income','tag_ids','subject_id','classify_type','is_free','price','is_promotion','is_issue','is_check','check_remark','issue_time','promotion_price','is_apply','apply_start_time','apply_end_time','expiry_start_time','expiry_end_time','create_time','update_time']);
				$row->visible(['student']);
				$row->getRelation('student')->visible(['phone','nick_name','sex']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
}
