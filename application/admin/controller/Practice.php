<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Practice extends Backend
{
    
    /**
     * Practice模型对象
     * @var \app\admin\model\Practice
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Practice;
        $this->view->assign("typeStatusList", $this->model->getTypeStatusList());
    }
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $teacher = $this->getUserInfo($this->auth->id,$this->auth->getGroups()[0]['group_id']);
            $list = Db::name('course')->alias('c')
                ->join('order o','c.id = o.courseId')
                ->join('question_bank qb','qb.course_id = c.id')
                ->join('student s','o.userId = s.id')
                ->join('subject su','c.subject_id = su.id')
                ->join('grade g','s.grade = g.id')
                ->join('question_bank_user qbu','qb.id = qbu.bank')
                ->where(['o.status'=>'paid','c.teacher_id' => $teacher['id']])
                ->where($where)
                ->field('qbu.id,c.type_status,s.nick_name,su.subject_name,g.grade_name,c.difficulty,qbu.create_time')
                ->order('qbu.create_time','DESC')
                ->limit($offset, $limit)
                ->select();
            $total = count($list);
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        // ids 为 by_question_bank_user id
        $row = Db::name('question_bank_user')->alias('qbu')
                        ->join('question_user qu','qbu.student = qu.student and qbu.bank = qu.question_bank')
                        ->join('question q','qu.question = q.id')
                        ->join('question_bank qb','q.bank_id = qb.id')
                        ->where(['qbu.id' => $ids])
                        ->field('q.id,qb.name,q.type,q.score,q.q_info,q.difficulty,q.answer,q.comm,qu.my_answer,qu.type as qutype')
                        ->select();
        foreach ($row as &$v){
            $q_tiem = Db::name('question_item')->where('question_id',$v['id'])->select();
            $v['q_item'] = $q_tiem;
        }
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $this->view->assign('title',$row[0]['name']);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    private function getUserInfo($id,$groupID){
        if($groupID == 1){
            return $this->auth->getUserInfo();
        }
        $table = $this->getTable($groupID);
        return Db::name($table)->where('admin_id',$id)->find();
    }
    private function  getTable($groupID){
        $table = '';
        switch ($groupID){
            case '2': $table = 'teacher'; break;
            case '4': $table = 'helper'; break;
            case '6': $table = 'partner'; break;
        }
        return $table;
    }
}
