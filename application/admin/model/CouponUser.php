<?php

namespace app\admin\model;

use think\Model;


class CouponUser extends Model
{

    

    

    // 表名
    protected $name = 'coupon_user';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text'
    ];
    

    
    public function getStatusList()
    {
        return ['1' => __('Status 1'), '0' => __('Status 0'), '2' => __('Status 2'), '3' => __('Status 3')];
    }

    public function getStatusList2()
    {
        return ['1' => __('Status 1'), '0' => __('Status 0'),];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function student()
    {
        return $this->belongsTo('Student', 'uid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
