<?php

namespace app\admin\model;

use think\Model;


class Question extends Model
{

    

    

    // 表名
    protected $name = 'question';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
        'difficulty_text'
    ];
    

    
    public function getTypeList()
    {
        return ['just' => __('Type just'), 'choose' => __('Type choose'), 'mchoose' => __('Type mchoose')];
    }

    public function getDifficultyList()
    {
        return ['hard' => __('Difficulty hard'), 'mid' => __('Difficulty mid'), 'easy' => __('Difficulty easy')];
    }

    public function getQStatusList()
    {
        return ['0' => __('QStatus 0'), '1' => __('QStatus 1') ];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getDifficultyTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['difficulty']) ? $data['difficulty'] : '');
        $list = $this->getDifficultyList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function questionbank()
    {
        return $this->belongsTo('QuestionBank', 'bank_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
