<?php

namespace app\admin\model;

use think\Model;


class CommUser extends Model
{

    

    

    // 表名
    protected $name = 'comm_user';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text',
        'type_text'
    ];
    

    
    public function getStatusList()
    {
        return ['on' => __('Status on'), 'off' => __('Status off')];
    }

    public function getAppliedList()
    {
        return ['0' => __('Applied 0'), '1' => __('Applied 1')];
    }
    public function getTypeList()
    {
        return ['course' => __('Type course'), 'chapter' => __('Type chapter')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function course()
    {
        return $this->belongsTo('Course', 'fid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
