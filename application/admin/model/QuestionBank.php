<?php

namespace app\admin\model;

use think\Model;


class QuestionBank extends Model
{

    

    

    // 表名
    protected $name = 'question_bank';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];






    public function getBankStatusList()
    {
        return ['0' => __('BankStatus 0'), '1' => __('BankStatus 1'), ];
    }




    public function grade()
    {
        return $this->belongsTo('Grade', 'grade_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function subject()
    {
        return $this->belongsTo('Subject', 'subject_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function course()
    {
        return $this->belongsTo('Course', 'course_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function courselesson()
    {
        return $this->belongsTo('CourseLesson', 'lesson_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
