<?php

namespace app\admin\model;

use think\Model;


class Order extends Model
{

    

    

    // 表名
    protected $name = 'order';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text',
        'payment_text',
        'price_type_text'
    ];
    

    
    public function getStatusList()
    {
        return ['created' => __('Status created'), 'paid' => __('Status paid'), 'refunding' => __('Status refunding'), 'refunded' => __('Status refunded'), 'cancelled' => __('Status cancelled'), 'lock' => __('Status lock')];
    }

    public function getPaymentList()
    {
        return ['alipay' => __('Payment alipay'), 'wechat' => __('Payment wechat'), 'none' => __('Payment none')];
    }

    public function getPriceTypeList()
    {
        return ['RMB' => __('Price_type rmb'), 'Coin' => __('Price_type coin')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPaymentTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['payment']) ? $data['payment'] : '');
        $list = $this->getPaymentList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPriceTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['price_type']) ? $data['price_type'] : '');
        $list = $this->getPriceTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function cashflow()
    {
        return $this->belongsTo('CashFlow', 'cash_sn', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function student()
    {
        return $this->belongsTo('Student', 'userId', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function course()
    {
        return $this->belongsTo('Course', 'courseId', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
