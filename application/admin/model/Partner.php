<?php

namespace app\admin\model;

use think\Model;


class Partner extends Model
{

    

    

    // 表名
    protected $name = 'partner';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
        'sex_text',
        'account_status_text'
    ];
    

    
    public function getTypeList()
    {
        return ['province' => __('Type province'), 'city' => __('Type city'), 'county' => __('Type county')];
    }

    public function getSexList()
    {
        return ['man' => __('Sex man'), 'woman' => __('Sex woman')];
    }

    public function getAccountStatusList()
    {
        return ['1' => __('Account_status 1'), '0' => __('Account_status 0')];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getSexTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sex']) ? $data['sex'] : '');
        $list = $this->getSexList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getAccountStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['account_status']) ? $data['account_status'] : '');
        $list = $this->getAccountStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
