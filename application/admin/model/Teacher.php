<?php

namespace app\admin\model;

use think\Model;


class Teacher extends Model
{

    

    

    // 表名
    protected $name = 'teacher';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'sex_text',
        'auth_status_text'
    ];
    

    
    public function getSexList()
    {
        return ['man' => __('Sex man'), 'woman' => __('Sex woman')];
    }

    public function getAuthStatusList()
    {
        return ['0' => __('Auth_status 0'), '1' => __('Auth_status 1'),'2' => __('Auth_status 2'),];
    }


    public function getSexTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sex']) ? $data['sex'] : '');
        $list = $this->getSexList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getAuthStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['auth_status']) ? $data['auth_status'] : '');
        $list = $this->getAuthStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
