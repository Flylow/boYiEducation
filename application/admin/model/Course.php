<?php

namespace app\admin\model;

use think\Model;


class Course extends Model
{

    

    

    // 表名
    protected $name = 'course';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_status_text',
        'is_free_text',
        'is_promotion_text',
        'is_issue_text',
        'is_check_text',
        'is_apply_text'
    ];
    

    protected static function init()
    {
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $row->getQuery()->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
        });
    }

    
    public function getTypeStatusList()
    {
        return ['1' => __('Type_status 1'), '2' => __('Type_status 2'), '3' => __('Type_status 3')];
    }

    public function getIsFreeList()
    {
        return ['0' => __('Is_free 0'), '1' => __('Is_free 1')];
    }

    public function getIsPromotionList()
    {
        return ['0' => __('Is_promotion 0'), '1' => __('Is_promotion 1')];
    }
    public function getIsDiscountList()
    {
        return ['0' => __('Is_discount 0'), '1' => __('Is_discount 1')];
    }
    public function getIsIssueList()
    {
        return ['0' => __('Is_issue 0'), '1' => __('Is_issue 1')];
    }

    public function getIsCheckList()
    {
        return ['0' => __('Is_check 0'), '1' => __('Is_check 1'), '2' => __('Is_check 2'), '3' => __('Is_check 3')];
    }

    public function getIsApplyList()
    {
        return ['0' => __('Is_apply 0'), '1' => __('Is_apply 1')];
    }

    public function getDifficultyList()
    {
        return ['1' => __('Difficulty 1'), '2' => __('Difficulty 2'), '3' => __('Difficulty 3'), '4' => __('Difficulty 4'), '5' => __('Difficulty 5')];
    }
    public function getTypeStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type_status']) ? $data['type_status'] : '');
        $list = $this->getTypeStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsFreeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_free']) ? $data['is_free'] : '');
        $list = $this->getIsFreeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsPromotionTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_promotion']) ? $data['is_promotion'] : '');
        $list = $this->getIsPromotionList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsIssueTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_issue']) ? $data['is_issue'] : '');
        $list = $this->getIsIssueList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsCheckTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_check']) ? $data['is_check'] : '');
        $list = $this->getIsCheckList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsApplyTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_apply']) ? $data['is_apply'] : '');
        $list = $this->getIsApplyList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function teacher()
    {
        return $this->belongsTo('Teacher', 'teacher_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function helper()
    {
        return $this->belongsTo('Helper', 'help_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function classify()
    {
        return $this->belongsTo('Classify', 'classify_type', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function tag()
    {
        return $this->belongsTo('Tag', 'tag_ids', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
