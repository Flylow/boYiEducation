<?php

namespace app\admin\model;

use think\Model;


class CourseLesson extends Model
{

    // 表名
    protected $name = 'course_lesson';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'lesson_status_text'
    ];
    

    
    public function getLessonStatusList()
    {
        return ['0' => __('Lesson_status 0'), '1' => __('Lesson_status 1')];
    }

    public function getIsOptionalList()
    {
        return ['0' => __('Is_optional 0'), '1' => __('Is_optional 1')];
    }
    public function getLessonTypeList()
    {
        return ['document' => __('Lesson_type document'), 'video' => __('Lesson_type video'), 'live' => __('Lesson_type live')];
    }
    public function getLessonStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['lesson_status']) ? $data['lesson_status'] : '');
        $list = $this->getLessonStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function course()
    {
        return $this->belongsTo('Course', 'course_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
