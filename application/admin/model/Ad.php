<?php

namespace app\admin\model;

use think\Model;


class Ad extends Model
{

    

    

    // 表名
    protected $name = 'ad';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text',
        'location_text'
    ];
    

    
    public function getStatusList()
    {
        return ['off' => __('Status off'), 'on' => __('Status on'), 'delete' => __('Status delete')];
    }

    public function getLocationList()
    {
        return ['top' => __('Location top'), 'bottom' => __('Location bottom'), 'left' => __('Location left'), 'right' => __('Location right')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getLocationTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['location']) ? $data['location'] : '');
        $list = $this->getLocationList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function course()
    {
        return $this->belongsTo('Course', 'course_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


}
