<?php

namespace app\admin\model;

use think\Model;


class Helper extends Model
{

    

    

    // 表名
    protected $name = 'helper';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'sex_text',
        'status_text'
    ];
    

    
    public function getSexList()
    {
        return ['man' => __('Sex man'), 'woman' => __('Sex woman')];
    }

    public function getStatusList()
    {
        return ['1' => __('Account_Status 1'), '0' => __('Account_Status 0')];
    }


    public function getSexTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sex']) ? $data['sex'] : '');
        $list = $this->getSexList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
