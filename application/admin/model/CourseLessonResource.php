<?php

namespace app\admin\model;

use think\Model;


class CourseLessonResource extends Model
{

    

    

    // 表名
    protected $name = 'course_lesson_resource';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function courselesson()
    {
        return $this->belongsTo('CourseLesson', 'lesson_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
