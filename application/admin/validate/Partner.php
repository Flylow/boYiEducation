<?php

namespace app\admin\validate;

use think\Validate;

class Partner extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'phone'    => 'require|regex:/^1[3456789]\d{9}$/',
        'password' => 'require|regex:/^[0-9a-z_.]{8,16}$/i',
    ];
    /**
     * 提示消息
     */
    protected $message = [
        'phone.require'     => '手机号码必填',
        'phone.regex'       => '手机号码格式错误',
        'password.require'  => '密码为必填',
        'password.regex'    => '密码格式为8-16位大小写字母、数字、特殊符号',
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => ['phone','password'],
        'edit' => ['phone','password'],
    ];
    
}
