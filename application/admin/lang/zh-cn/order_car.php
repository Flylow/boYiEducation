<?php

return [
    'Id'                       => '主键',
    'Name'                     => '购物车名称',
    'Uid'                      => '学生标识',
    'Remark'                   => '备注',
    'User_name'                => '学生昵称',
    'Ordercaritem.course_name' => '课程名称'
];
