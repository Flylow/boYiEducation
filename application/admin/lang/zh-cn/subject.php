<?php

return [
    'Id'               => '编号',
    'Subject_name'     => '学科名称',
    'Grade_ids'        => '年级编号组',
    'Create_time'      => '创建时间',
    'Image'            => 'logo',
    'Grade.grade_name' => '年级名称'
];
