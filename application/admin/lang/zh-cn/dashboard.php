<?php

return [
    'Custom'                  => '自定义',
    'Pid'                     => '父ID',
    'Type'                    => '栏目类型',
    'Image'                   => '图片',
    'Total user'              => '总会员数',
    'Total view'              => '总访问数',
    'Total order'             => '总订单数',
    'Total order amount'      => '总金额',
    'Today user signup'       => '今日注册',
    'Today user login'        => '今日登录',
    'Today order'             => '今日订单',
    'Unsettle order'          => '未处理订单',
    'Seven dnu'               => '七日新增',
    'Seven dau'               => '七日活跃',
    'Custom zone'             => '这里是你的自定义数据',
    'Sales'                   => '成交数',
    'Orders'                  => '订单数',
    'Real time'               => '实时',
    'Category count'          => '分类统计',
    'Category count tips'     => '当前分类总记录数',
    'Attachment count'        => '附件统计',
    'Attachment count tips'   => '当前上传的附件数量',
    'Article count'           => '文章统计',
    'News count'              => '新闻统计',
    'Comment count'           => '评论次数',
    'Like count'              => '点赞次数',
    'Recent news'             => '最新新闻',
    'Recent discussion'       => '最新发贴',
    'Server info'             => '服务器信息',
    'PHP version'             => 'PHP版本',
    'Fastadmin version'       => '主框架版本',
    'Fastadmin addon version' => '插件版本',
    'Thinkphp version'        => 'ThinkPHP版本',
    'Sapi name'               => '运行方式',
    'Debug mode'              => '调试模式',
    'Software'                => '环境信息',
    'Upload mode'             => '上传模式',
    'Upload url'              => '上传URL',
    'Upload cdn url'          => '上传CDN',
    'Cdn url'                 => '静态资源CDN',
    'Timezone'                => '时区',
    'Language'                => '语言',
    'View more'               => '查看更多',
    'Name'                 => '姓名',
    'Phone'                => '手机号',
    'Password'             => '密码',
    'Nick_name'            => '昵称',
    'Header'               => '头像',
    'Point'                => '奖励',
    'Sex'                  => '性别',
    'Sex man'              => '男',
    'Sex woman'            => '女',
    'Age'                  => '年龄',
    'Last_login_time'      => '最后登录时间',
    'New_message_num'      => '未读私信数',
    'New_notification_num' => '未读消息数',
    'Register_time'        => '注册时间',
    'Invite_code'          => '邀请码',
    'English_name'         => '英文名',
    'Birthday'             => '出生年月',
    'Address'              => '收货地址',
    'Grade'                => '年级',
    'School'               => '学校',
    'Admin_id'             => '登录标识',
    'Auth_status'          => '审核状态',
    'Auth_status 0'        => '未审核',
    'Auth_status 1'        => '审核通过',
    'Auth_status 2'        => '审核不通过',
    'Create_time'          => '注册时间',
    'Auth_time'            => '认证通过时间',
    'School_age'           => '教龄',
    'Certification'        => '教师资格证',
    'Self_know'            => '自我介绍',
    'Honor_know'           => '荣誉介绍',
    'Other_materials'      => '其他辅助材料',
];
