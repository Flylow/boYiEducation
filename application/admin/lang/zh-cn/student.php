<?php

return [
    'Id'                   => '学生标识',
    'Phone'                => '手机号',
    'Password'             => '密码',
    'Nick_name'            => '昵称',
    'Header_image'         => '头像',
    'Point'                => '积分',
    'Sex'                  => '性别',
    'Sex man'              => '男',
    'Sex woman'            => '女',
    'Age'                  => '年龄',
    'Last_login_time'      => '最后登录时间',
    'New_message_num'      => '未读私信数',
    'New_notification_num' => '未读消息数',
    'Register_time'        => '注册时间',
    'Invite_code'          => '邀请码',
    'User_name'            => '姓名',
    'Course_name'          => '目前所学课程',
    'Plan'                 => '学习进度',
    'English_name'         => '英文名',
    'Birthday'             => '出生年月',
    'Address'              => '收货地址',
    'Grade'                => '年级',
    'School'               => '就读学校'
];
