<?php

return [
    'Id'          => '主键',
    'Name'        => '名称',
    'Weight'      => '权重',
    'Create_time' => '创建时间',
    'Create_user' => '创建者'
];
