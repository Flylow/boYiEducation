<?php

return [
    'Id'          => '自增编号',
    'Tag_name'    => '标签名称',
    'Pid'         => '父级编号',
    'Create_time' => '创建时间',
    'Update_time' => '修改时间'
];
