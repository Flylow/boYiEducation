<?php

return [
    'Bank_id'           => '题库名',
    'Type'              => '题目类型',
    'Type just'         => '判断题',
    'Type choose'       => '单选题',
    'Type mchoose'      => '多选题',
    'Score'             => '分数',
    'Info'              => '题干',
    'Q_item'            => ' 题项标识集合',
    'Q_info'            => '题干',
    'Difficulty'        => '难易程度',
    'Difficulty hard'   => '困难',
    'Difficulty mid'    => '适中',
    'Difficulty easy'   => '容易',
    'QStatus'           => '题目状态',
    'QStatus 0'         => '下线',
    'QStatus 1'         => '上线',
    'Create_user'       => '出题人标识',
    'Create_time'       => '出题时间',
    'Answer'            => '答案标识',
    'Comm'              => '解释',
    'Questionbank.name' => '题库名称'
];
