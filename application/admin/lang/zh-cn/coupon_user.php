<?php

return [
    'Coupon_id'         => '主键',
    'Name'              => '优惠券名称',
    'Uid'               => '学生标识',
    'Sn'                => '券码',
    'Status'            => '状态',
    'Status 1'          => '有效',
    'Status 0'          => '无效',
    'Status 2'          => '锁定',
    'Status 3'          => '已使用',
    'Money'             => '优惠券金额',
    'Create_time'       => '创建时间',
    'Valid_time'        => '截止有效期',
    'Student.nick_name' => '昵称'
];
