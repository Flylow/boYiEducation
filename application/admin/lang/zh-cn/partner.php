<?php

return [
    'Id'                   => '主键',
    'Type'                 => '类型',
    'Type province'        => '省代理',
    'Type city'            => '市代理',
    'Type county'          => '县代理',
    'Phone'                => '手机号',
    'Password'             => '密码',
    'Nick_name'            => '昵称',
    'Header'               => '头像',
    'Money'                => '金额',
    'Sex'                  => '性别',
    'Sex man'              => '男',
    'Sex woman'            => '女',
    'Age'                  => '年龄',
    'Last_login_time'      => '最后登录时间',
    'New_message_num'      => '未读私信数',
    'New_notification_num' => '未读消息数',
    'Register_time'        => '注册时间',
    'Invite_code'          => '邀请码',
    'User_name'            => '姓名',
    'Remark'               => '备注',
    'Account_status'       => '账号状态',
    'Account_status 1'     => '正常',
    'Account_status 0'     => '冻结'
];
