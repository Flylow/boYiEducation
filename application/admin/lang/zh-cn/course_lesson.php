<?php

return [
    'Id'                       => '编号',
    'Course_id'                => '课程编号',
    'Lesson_id'                => '课时序号',
    'Title'                    => '标题',
    'Room_id'                  => '云信直播房间号',
    'Content'                  => '章节正文',
    'Media_uri'                => '媒体文件',
    'Media_id'                 => '媒体id',
    'Learned_num'              => '已学的学员数',
    'Lesson_status'            => '章节状态',
    'Lesson_status 0'          => '未发布',
    'Lesson_status 1'          => '发布',
    'Lesson_status 2'          => '取消发布',
    'Start_time'               => '直播课开始时间',
    'End_time'                 => '直播课结束时间',
    'Is_optional'              => '选修',
    'Is_optional 0'            => '否',
    'Is_optional 1'            => '是',
    'Lesson_type'              => '课时类型',
    'Lesson_type document'     => '图文',
    'Lesson_type video'        => '视频',
    'Lesson_type live'         => '直播',
    'Min_length'               => '最少学习时长',
    'Video_length'             => '视频时长',
    'Course.title'             => '课程标题'
];
