<?php

return [
    'Name'                 => '姓名',
    'Phone'                => '手机号',
    'Password'             => '密码',
    'Nick_name'            => '昵称',
    'Header'               => '头像',
    'Point'                => '奖励',
    'Sex'                  => '性别',
    'Sex man'              => '男',
    'Sex woman'            => '女',
    'Age'                  => '年龄',
    'Last_login_time'      => '最后登录时间',
    'New_message_num'      => '未读私信数',
    'New_notification_num' => '未读消息数',
    'Register_time'        => '注册时间',
    'Invite_code'          => '邀请码',
    'English_name'         => '英文名',
    'Birthday'             => '出生年月',
    'Address'              => '收货地址',
    'Grade'                => '年级',
    'School'               => '就读学校',
    'Admin_id'             => '登录标识',
    'Auth_status'          => '审核状态',
    'Auth_status 0'        => '未审核',
    'Auth_status 1'        => '审核通过',
    'Auth_status 2'        => '审核不通过',
    'Auth_status 3'        => '待审核',
    'Auth_status 4'        => '开始使用',
    'Create_time'          => '注册时间',
    'School_age'           => '教龄',
    'Certification'        => '教师资格证',
    'Self_know'            => '自我介绍',
    'Honor_know'           => '荣誉介绍',
    'Auth_time'            => '认证通过时间'
];
