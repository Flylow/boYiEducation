<?php

return [
    'Name'          => '名称',
    'Pic_url'       => '图片地址',
    'Status'        => '状态',
    'Status off'    => '下线',
    'Status on'     => '上线',
    'Status delete' => '删除',
    'Create_time'   => '创建时间',
    'Create_user'   => '创建者',
    'Link'          => '链接',
    'Update_time'   => '更新时间',
    'Update_user'   => '更新者',
    'Ba_comment'    => '备注'
];
