<?php

return [
    'Id'                     => '编号',
    'Invest_id'              => '分销编号',
    'Earnings'               => '收益(积分/金钱)',
    'Create_time'            => '创建时间',
    'Order_id'               => '订单编号',
    'Type'                   => '类型',
    'Type 1'                 => '注册',
    'Type 2'                 => '订单',
    'Invest.uid'             => '推荐人标识',
    'Invest.role'            => '角色',
    'Invest.role teacher'    => '教师',
    'Invest.role student'    => '学生',
    'Invest.role helper'     => '助教',
    'Invest.role partner'    => '合伙人',
    'Invest.to_uid'          => '被推荐人id',
    'Invest.to_role'         => '角色',
    'Invest.to_role teacher' => '教师',
    'Invest.to_role student' => '学生',
    'Invest.to_role helper'  => '助教',
    'Invest.to_role partner' => '合伙人',
    'Invest.to_nick_name'    => '被推荐人名称'
];
