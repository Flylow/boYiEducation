<?php

return [
    'Id'              => '主键',
    'Name'            => '名称',
    'Pic_url'         => '图片地址',
    'Ad_comment'      => '备注',
    'Create_time'     => '创建时间',
    'Update_time'     => '更新时间',
    'Create_user'     => '创建者',
    'Update_user'     => '更新者',
    'Status'          => '状态',
    'Status off'      => '下线',
    'Status on'       => '上线',
    'Status delete'   => '删除',
    'Location'        => '位置',
    'Location top'    => '顶部',
    'Location bottom' => '底部',
    'Location left'   => '左侧',
    'Location right'  => '右侧',
    'CourseId'        => '课程',
    'Course.title'    => '课程标题'

];
