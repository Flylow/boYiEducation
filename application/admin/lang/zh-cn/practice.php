<?php

return [
    'Id'                           => '主键',
    'Type_status'                  => '课程类型',
    'Type_status 1'                => '直播课',
    'Type_status 2'                => '录播课',
    'Type_status 3'                => '公开课',
    'Difficulty'                   => '难度',
    'Difficulty 1'                 => '★',
    'Difficulty 2'                 => '★★',
    'Difficulty 3'                 => '★★★',
    'Difficulty 4'                 => '★★★★',
    'Difficulty 5'                 => '★★★★★',
    'nick_name'                    => '答题者',
    'Grade'                        => '年级',
    'Subject_name'                 => '科目',
    'Create_time'                  => '答题时间'
];
