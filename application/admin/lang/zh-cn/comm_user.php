<?php

return [
    'Id'           => '主键',
    'Uid'          => '评论者标识',
    'Uname'        => '评论者姓名',
    'Content'      => '评论内容',
    'Time'         => '评论时间',
    'Status'       => '状态',
    'Status on'    => '可见',
    'Status off'   => '不可见',
    'Applied'   => '回复状态',
    'Applied 0'   => '未回复',
    'Applied 1'   => '已回复',
    'Type'         => '类型',
    'Type course'  => '课程',
    'Type chapter' => '章节',
    'Fid'          => '关联外键id',
    'Remark'       => '备注',
    'Level'        => '课程体验',
    'Pid'          => '父评论标识',
    'Course.title' => '课程标题'
];
