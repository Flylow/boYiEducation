<?php

return [
    'Id'                   => '主键',
    'Name'                 => '姓名',
    'Phone'                => '手机号',
    'Password'             => '密码',
    'Nick_name'            => '昵称',
    'Header'               => '头像',
    'Money'                => '金额',
    'Sex'                  => '性别',
    'Sex man'              => '男',
    'Sex woman'            => '女',
    'Age'                  => '年龄',
    'Last_login_time'      => '最后登录时间',
    'New_message_num'      => '未读私信数',
    'New_notification_num' => '未读消息数',
    'Create_time'        => '添加时间',
    'Invite_code'          => '邀请码',
    'User_name'            => '姓名',
    'English_name'         => '英文名',
    'Account_Status'               => '账户状态',
    'Account_Status 1'        => '正常',
    'Account_Status 0'        => '冻结',
    'User_id'              => '前端登录标识',
    'Admin_id'             => '后端登录标识',
    'Teacher_Id'             => '教师标识'
];
