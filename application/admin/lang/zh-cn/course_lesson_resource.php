<?php

return [
    'Id'                 => '主键',
    'Name'               => '资源名称',
    'Lesson_id'          => '课时id',
    'Type'               => '资源类型',
    'Location'           => '资源路径',
    'Size'               => '资源大小',
    'Remark'             => '资源描述',
    'Create_time'        => '上传时间',
    'Courselesson.title' => '标题'
];
