<?php

return [
    'Id'                   => '主键',
    'Name'                 => '名称',
    'Create_user'          => '创建者',
    'Create_time'          => '创建时间',
    'Course_id'            => '课程标识',
    'Lesson_id'            => '章节标识',
    'Bank_tags'            => '标签标识集合',
    'Grade_id'             => '年级标识',
    'Subject_id'           => '学科标识',
    'BankStatus'   => '题库状态',
    'BankStatus 0'   => '下线',
    'BankStatus 1'   => '上线',
    'Grade.grade_name'     => '年级名称',
    'Subject.subject_name' => '学科名称',
    'Course.title'         => '课程',
    'Courselesson.title'   => '子章节'
];
