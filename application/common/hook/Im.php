<?php
namespace app\common\hook;


use app\common\controller\ServerAPI;
use app\common\controller\Util;
use think\Db;
use think\Log;

class Im
{
    private static $IM;

    public function __construct()
    {
        Im::getIm();
    }

    /**
     * Notes:创建云信账号
     * User: XingLiangRen
     * Date: 2020/8/24
     * Time: 17:56
     * @param $data
     * @return false|int|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function createAccountForUser($data){
       self::checkOnly($data);
       $result =  self::$IM->createUserId(self::createNewAccid(),$data['name']);
       if($result['code'] == 200){
           $insertData = [
               'uid'   => $data['uid'],
               'type'  => $data['type'],
               'accid' => $result['info']['accid'],
               'token' => $result['info']['token'],
           ];
           return  Db::name('im')->insert($insertData);
       }else{
           $data['createTime'] = date('Y-m-d H:i:s');
           $data['httpInfo'] = $result;
           Log::error(\GuzzleHttp\json_encode($data));
       }
       return false;
    }

    /**
     * Notes:更新用户名片
     * User: XingLiangRen
     * Date: 2020/8/26
     * Time: 10:52
     * @param $data  uid,type,name,
     */
    public static function updateUserInfo($data){
        $accid = Db::name('im')->where(['uid'=>$data['uid'],'type'=>$data['type']])->field('accid')->find()['accid'];
        if(!Util::checkEmpty($accid)){
            return false;
        }
        $data = Util::setDefault($data,['name','icon','sign','email','birth','mobile','ex','gender']);
        $result = self::$IM->updateUinfo($accid,$data['name'],$data['icon'],$data['sign'],$data['email'],$data['birth'],$data['mobile'],$data['gender'] == '' ? 0: $data['gender']);
        Log::write($result);
        if($result['code'] == 200){
           return  true;
        }
        return  false;
    }

    /**
     * Notes:检测是否用户账号是否唯一
     * User: XingLiangRen
     * Date: 2020/8/26
     * Time: 10:45
     * @param $data
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    private static function checkOnly($data){
        $only =  Db::name('im')->where(['uid' => ['=',$data['uid']], 'type' => ['=',$data['type']]])->find();
        if(Util::checkEmpty($only,'array')){
            return  false;
        }
    }
    /*
     * 创建聊天室
     */
    public static function createChatroom($data){
      return self::$IM->createChatroom($data['creator'],$data['name']);
    }

    /**
     * 关闭或打开聊天室
     */
    public static function toggleCloseStat($data){
        return self::$IM->toggleCloseStat($data['roomid'],$data['operator'],$data['valid']);
    }

    /**
     * Notes:获取聊天室信息
     * User: XingLiangRen
     * Date: 2020/9/16
     * Time: 13:43
     */
    public static function getChannel($data){
      return self::$IM->requestAddr($data['roomid'],$data['accid']);
    }

    public static function getIm(){
        if(!self::$IM instanceof ServerAPI){
             self::$IM = new ServerAPI();
        }
        return self::$IM;
    }

    public static function createNewAccid(){
        return md5(time() . rand(00000,99999));
    }
}