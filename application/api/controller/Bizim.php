<?php


namespace app\api\controller;

use app\common\controller\Api;
use app\common\controller\ServerAPI;
use think\Db;
use think\Hook;
// 允许跨域
header('Access-Control-Allow-Origin:*');//允许跨域
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Headers:x-requested-with,content-type,token,appkey');//浏览器页面ajax跨域请求会请求2次，第一次会发送OPTIONS预请求,不进行处理，直接exit返回，但因为下次发送真正的请求头部有带token，所以这里设置允许下次请求头带token否者下次请求无法成功
    exit("ok");
}
class Bizim extends Api
{
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['Test','createChatroom','getAddress','toggleCloseStat'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    public function Test()
    {

    }
    public function getAddress(){
        $rommid = input('roomid');
        if(empty($rommid)){
            $this->error('roomid is empty');
        }
        $accid = input('accid');
        if(empty($accid)){
            $this->error('accid is empty');
        }
        $hooks_data = [
            'roomid' => $rommid,
            'accid'  => $accid,
        ];
        $data = Hook::listen("getChannel",$hooks_data,'',true);
       $this->success($data);
    }

    public function createChatroom(){
        $creator = input('creator');
        if(empty($creator)){
            $this->error('creator is empty');
        }
        $name = input('name');
        if(empty($name)){
            $this->error('name is empty');
        }
        $hooks_data = [
            'creator' => $creator,
            'name'     => $name,
        ];
        $data = Hook::listen("createChatroom",$hooks_data,'',true);
        $this->success($data);
    }

    /**
     * Notes: 关闭或打开聊天室
     * User: XingLiangRen
     * Date: 2020/9/16
     * Time: 14:58
     */
    public function toggleCloseStat(){
        $roomid = input('roomid');
        if(empty($roomid)){
            $this->error('roomid is empty');
        }
        $operator = input('operator');
        if(empty($operator)){
            $this->error('operator is empty');
        }
        $valid = input('valid','false');
        $hooks_data = [
            'roomid'       => $roomid,
            'operator'     => $operator,
            'valid'        => $valid,
        ];
        $data = Hook::listen("toggleCloseStat",$hooks_data,'',true);
        $this->success($data);
    }
    /**
     * Notes:获取学生im账号
     * User: XingLiangRen
     * Date: 2020/9/15
     * Time: 13:50
     */
    public function getAccount(){
        $student = Db::name('student')->alias('s')
                        ->join('im i','i.uid = s.id')
                        ->where(['user_id' => $this->auth->id,'type' => 'student'])
                        ->field('i.accid,i.token')
                        ->find();
         $this->success("查询成功",$student);
    }
}