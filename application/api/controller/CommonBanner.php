<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Db;

/**
 * 示例接口
 */
class CommonBanner extends Api
{

    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['myList'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];


    /** 列表查询
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function myList()
    {
        $List = Db::name("banner")->where('status','=','on')->order("id desc")->select();
        $this->success("ok",$List);
    }

}
