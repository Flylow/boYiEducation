<?php


namespace app\api\controller;

use app\common\controller\Api;
use app\common\controller\Util;
use PhpOffice\PhpSpreadsheet\Calculation\DateTime;
use think\Db;

class Bizcourse extends Api
{
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['register','changeCourseStatus'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    /**
     * Notes:课程列表
     * User: XingLiangRen
     * Date: 2020/9/3
     * Time: 12:29
     */
    public function  getCourseList(){
        $userInfo = Util::getUserInfo($this->auth->id);
        $where['teacher_id'] = $userInfo['id'];
        //type_status 公开课等
        $type_status = $this->request->get("type_status");
        if(Util::checkEmpty($type_status)){
            $where['type_status'] = $type_status;
        }
        $data = Db::name('course')->where($where)->field('id,title')->select();
        $this->success('查询成功',$data);
    }
    /**
     * Notes:获取课程资源
     * User: XingLiangRen
     * Date: 2020/9/7
     * Time: 17:28
     */
    public function getCourseResource(){
        $courseID = $this->request->get('courseID');
        $data = Db::name('course_lesson_resource')->alias('clr')
                        ->join('course_lesson cl','cl.id = clr.lesson_id','LEFT')
                        ->where('cl.course_id',$courseID)
                        ->field('clr.*')
                        ->select();
        $this->success("查询成功",$data);
    }

    /**
     * Notes:定时任务执行改变课程状态
     * User: XingLiangRen
     * Date: 2020/9/9
     * Time: 14:55
     */
    public function changeCourseStatus(){
        $data = Db::name('course')->where(['is_apply' => '1','apply_end_time'=> ['>=',date('Y-m-d')]])->update(['is_apply'=>'0']);
        Util::write_log($data);
    }
}