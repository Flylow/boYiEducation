<?php

namespace app\api\controller;

use app\admin\model\CommUser;
use app\admin\model\Student;
use app\common\controller\Api;
use app\common\controller\Util;
use think\Db;

/**
 * 示例接口
 */
header('Access-Control-Allow-Origin:*');//允许跨域
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Headers:x-requested-with,content-type,token');//浏览器页面ajax跨域请求会请求2次，第一次会发送OPTIONS预请求,不进行处理，直接exit返回，但因为下次发送真正的请求头部有带token，所以这里设置允许下次请求头带token否者下次请求无法成功
    exit("ok");
}

class Bizcommuser extends Api
{

    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['getCourseCommList'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    /**  学生添加课程评论
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * courseId 课程标识
     * content 评论内容
     * level 星级 1-5
     */
    public function addCommon()
    {
        $param = input();

        $courseId = $param['courseId'];
        $content = $param['content'];
        $level = $param['level'];
        $user_id = $this->auth->id;
        if (empty($user_id)) {
            return $this->error('请先登录');
        }
        $student = Student::get(['user_id' => $user_id]);
        if (!$student) {
            return $this->error('用户不存在');
        }
        $comm = CommUser::get(['uid' => $student['id'],
            'fid' => $courseId]);
        if ($comm) {
            return $this->error('已经评论过了');
        }
        $myinsert = [
            'uid' => $student['id'],
            'uname' => $student['nick_name'],
            'content' => $content,
            'fid' => $courseId,
            'level' => $level,
            'time' => Util::getNowDateTime()
        ];
        $myinsert = CommUser::create($myinsert);
        return $this->success('OK', $myinsert);
    }

    /***
     * @Notes:
     * @Author: zhao
     * @Date: 2020/9/4
     * @Time: 15:08
     * @Interface getCourseCommList
     * 获取课程的评论
     */
    public function getCourseCommList()
    {
        $param = input();
        $courseId = $param['id'];
        $page = input('page', 0);
        $num = input('num', '5');

        if (!$courseId) {
            return $this->error('课程不存在');
        }
        $mywhere = [
            'fid' => $courseId,
            'status' => 'on',
            'pid' => 0
        ];
        $commList = Db::name('comm_user')->field(['id','uid', 'uname', 'content', 'time', 'level'])->where($mywhere)->page($page, $num)->select();
        foreach ($commList as $key => &$value) {
            $comm = CommUser::field(['uname', 'content', 'time'])->where(['pid' => $value['id']])->select();
            if (!empty($comm)) {
                $value['reply'] = $comm;
            } else {
                $value['reply'] = array();
            }
            $student = Db::name('student')->where('id',$value['uid'])->find();

            $value['header']=$student['header_image'];
        }

        $total = Db::name('comm_user')->where($mywhere)->count();
        $result = array('total' => $total, 'commList' => $commList);
        return $this->success('ok', $result);
    }
}
