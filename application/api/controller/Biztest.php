<?php


namespace app\api\controller;


use app\api\ApiHandle;

class Biztest extends ApiHandle
{

    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['index'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = [];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Student;
    }

    public function index()
    {

        $filter = [
            'id' => 20
        ];
        $op = [
            'id' =>"="
        ];
        $this->success('查询成功',$this->indexed());
    }
}