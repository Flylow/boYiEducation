<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// 应用行为扩展定义文件
return [
    // 应用初始化
    'app_init'     => [],
    // 应用开始
    'app_begin'    => [],
    // 应用调度
    'app_dispatch' => [
        'app\\common\\behavior\\Common',
    ],
    // 模块初始化
    'module_init'  => [
        'app\\common\\behavior\\Common',
    ],
    // 插件开始
    'addon_begin'  => [
        'app\\common\\behavior\\Common',
    ],
    // 操作开始执行
    'action_begin' => [],
    // 视图内容过滤
    'view_filter'  => [],
    // 日志写入
    'log_write'    => [],
    // 应用结束
    'app_end'      => [],
    //分销
    'calculatePoint'       => [
        'app\\api\\hook\\Retail',
    ],
//        分销关联关系
    'createTeacherInvest'       => [
        'app\\api\\hook\\Retail',
    ],
//    教师助教区域合伙人分销
    'commonDistribution'       => [
        'app\\api\\hook\\Retail',
    ],
    //网易云信
    'createAccountForUser'    => [
    'app\\common\\hook\\Im',
    ],
    'createChatroom'    => [
     'app\\common\\hook\\Im',
    ],
    'getChannel'    => [
    'app\\common\\hook\\Im',
    ],
     'toggleCloseStat'    => [
    'app\\common\\hook\\Im',
    ],
    'updateUserInfo'    => [
        'app\\common\\hook\\Im',
    ],

];
