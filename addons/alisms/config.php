<?php

return array (
  0 => 
  array (
    'name' => 'key',
    'title' => '应用key',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'LTAIdq1EPbg6J9K2',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'secret',
    'title' => '密钥secret',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'mUThzcu071Ep4jGlM9gC2i8DPw0EED',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  2 => 
  array (
    'name' => 'sign',
    'title' => '签名',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '坤渔科技',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  3 => 
  array (
    'name' => 'template',
    'title' => '短信模板',
    'type' => 'array',
    'content' => 
    array (
    ),
    'value' => 
    array (
      'register' => 'SMS_146809121',
      'resetpwd' => 'SMS_146804144',
      'changemobile' => 'SMS_146804145',
      'mobilelogin' => 'SMS_200694225',
    ),
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  4 => 
  array (
    'name' => '__tips__',
    'title' => '温馨提示',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '应用key和密钥你可以通过 https://ak-console.aliyun.com/?spm=a2c4g.11186623.2.13.fd315777PX3tjy#/accesskey 获取',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
);
