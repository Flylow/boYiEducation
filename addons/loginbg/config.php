<?php

return array (
  0 => 
  array (
    'name' => 'mode',
    'title' => '模式',
    'type' => 'radio',
    'content' => 
    array (
      'fixed' => '固定',
      'random' => '每次随机',
      'daily' => '每日切换',
    ),
    'value' => 'fixed',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'image',
    'title' => '固定背景图',
    'type' => 'image',
    'content' => 
    array (
    ),
    'value' => '/uploads/20200814/742fa4d44b68874fcada0e6672c4d802.jpg',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
);
