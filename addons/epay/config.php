<?php

return array (
  0 => 
  array (
    'name' => 'wechat',
    'title' => '微信',
    'type' => 'array',
    'content' => 
    array (
    ),
    'value' => 
    array (
      'appid' => 'wx26b4940a20a95135',
      'app_id' => 'wx26b4940a20a95135',
      'app_secret' => 'dc09ec3bc0f5d5ba30bb5e5d93e660f4',
      'miniapp_id' => '',
      'mch_id' => '1576766511',
      'key' => '3178ec56cba13f69cdc95fe252005d3c',
      'notify_url' => '/addons/epay/api/notifyx/type/wechat',
      'cert_client' => '/epay/certs/apiclient_cert.pem',
      'cert_key' => '/epay/certs/apiclient_key.pem',
      'log' => '1',
    ),
    'rule' => '',
    'msg' => '',
    'tip' => '微信参数配置',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'alipay',
    'title' => '支付宝',
    'type' => 'array',
    'content' => 
    array (
    ),
    'value' => 
    array (
      'app_id' => '2021001188677488',
      'notify_url' => '/addons/epay/api/notifyx/type/alipay',
      'return_url' => '/addons/epay/api/returnx/type/alipay',
      'ali_public_key' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAimKPsDtX0ECCLLUQIfYmhgBU+PF0bFOiVqSHZ0ETbo4gXWTEeHtqyoDGOur6EGHDyniy8dJSnURGce+vxOWInuHOZEUM1YeLv256FcS8iCDQ/PrM+OrjvdyyDtjhxpNwHI7t6ZhIetzcmBVL5l31VcgcH18dwRZfOFds0eNguBNCBiooZiMM/Sk531siqXOkgSYzSXol6nGNTx60NBhfLB7OkopyK6oWpgUP+au4nzj7arn0oxZyiCvKf+URqndNpGRQfgXGUL4qGbkgMOSkqStxJkZ7oX+rnDMQdruo1iS3OGOiiWlCSnK8Zdb8+QX6qAjfxtEq9H9imJQUxxXZ9QIDAQAB  ',
      'private_key' => 'MIIEogIBAAKCAQEAhXab84MYASw93IgqmSrVq69xmMHueQnd6FJUonH6iXdn6831xeCXM4y4/M4iQD2rV+4orq2tzhBJ73ZCMu38bYgOaNMCZALpp+d9AQPwVJ3sru9vacZNirC04zJrR0ry1Ns+bFdkhuPL/2uE/EGMHr53vdCbGi8owsbcYghS5D4F0bHWErXc+IFzZ65Dx+GLI+fxaKT/et7LvdjTFfjSQS9muqWqVj133rNT7c8QB8MKUWas9waeUFMktaDojPb3Xrhw8+w83z165l+5nDEml4wJKZ1j7B+802W7YOLWiZGD2PE6Grhpt0xQpHcEOC2/Wu7MmPzbVwLgwxwulOW3qQIDAQABAoIBABWbWDAaPx6hfPphCRR2/NSouKlUbDkoQcbE3XLXJQpwTGC+nVWFwLTVyXUUIjNoSH8ZzXhKIeNEe9PKv3jjUoShDEQoNTDfz9gg//nUG2p3jsPbe5wSQKd9aCuIFBocpieTvw1nMJ0t7+7QB12XdwnbKCOQzCt3CxBNhYxRcKoGrEMyfVnDVWghicMaPkAsGrg4vanug/vmbVrPoZo3vy+mpw1ZvnkdqFxzMIv3l7EXRFX7q+nTg7zKQRmo46fjTtEypd8KpKVW56k65oQ1UmbuxADNnGXN0JnwnWryxNv01vPjvMJaQxNDzuIjrqmWmzBJMbockUzOD2GPTQ+2yUkCgYEAz9u069TxfPiiVCnP/y29AQqk1/FEMWqJwMwFvsfaA+7w8I5BNrmzwUu98hjc0Q4fwcHeRpEX3CNHgabni04XFIj2ejHaryCBI6/i5zD4e+BXvOWFKIuCOWL3rrcSEX3ntlyUvj3HQ18rTz5EwJ+JfWbS69Tgi8N3YN4ogOhhN4MCgYEApF/ke+vPr+BvEsasxcY9kB9zskcqbAuLfnDDYtJoHypKp0xd/VB7ljECSnKlC0auG1B2KV2x0jzsCBvA6KO1BHUc8xkcUojw0cfe1jRPPHGXJxIcQaozfAN8ZaiaxoYlLBlSk1yaix1tiVaxQWThVNk4ir3g34pLji/5W5C5wGMCgYAOIFptZK181tEBc04Fg7BCr9yAKI9/5EO0bKk/TAnPFqAdWS2pcHl8+47PM01ej90sp+ZZWWcLuQIVF6jI2OfMp3pI17aLDGR6bmu3VqaslJSh699e4AxbphQ7NYnu0mW7CRO60iwGsiMZYZv+k1kewqw3BuKz3PbyCGcsS56/zQKBgH9jzej4VYpBzvNnRCk2vSmAJqNJIiiAPgitDc/9rzJQDXFg7wrOICnDK7pU5XI6wd5rWlguxAxiqK6UC8qAbV7aAKiqkleox7JdS2cJPUtj4TgngfymEqaO8Q68P8+R0Sc/LM8GGsOr9+NTa6Z5r2SZwIdOJwQ34tx8c62yGTNvAoGAC1wLDznK3T9VwbRv5KhFF3ZwQxS3tohiUyK9+8pYZ+AUPR4mE2pi1lPSubw4aIzmh+sWsU0FVVgegUwIP3IdqFy9SnuS7lfRhaBaV8NDYOhWMQ+en909Q/VTP+3slO/sQrkYkwDOP73bPqYeLGg7bABv/FgvOgqDouoLXJSaf8E=',
      'log' => '1',
    ),
    'rule' => 'required',
    'msg' => '',
    'tip' => '支付宝参数配置',
    'ok' => '',
    'extend' => '',
  ),
  2 => 
  array (
    'name' => '__tips__',
    'title' => '温馨提示',
    'type' => 'array',
    'content' => 
    array (
    ),
    'value' => '请注意微信支付证书路径位于/addons/epay/certs目录下，请替换成你自己的证书<br>appid：APP的appid<br>app_id：公众号的appid<br>app_secret：公众号的secret<br>miniapp_id：小程序ID<br>mch_id：微信商户ID<br>key：微信商户支付的密钥',
    'rule' => '',
    'msg' => '',
    'tip' => '微信参数配置',
    'ok' => '',
    'extend' => '',
  ),
);
